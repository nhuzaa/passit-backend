[![Donate](https://img.shields.io/liberapay/patrons/Passit.svg?logo=liberapay")](https://liberapay.com/Passit/)
[![Gitter chat](https://badges.gitter.im/gitterHQ/gitter.png)](https://gitter.im/passit_community/community)

# Passit Backend

If you are looking to run your own passit server - go [here](https://passit.io/install/) instead.

This is the Django backend for passit.io. It's duty is to:

- Store user (encrypted) data
- Have no way to read the data ever - not even storing critital data in ram
- Authenticate users and ensure they can only access what they made or have access to.
- Run tests to verify the general security model

Please note that the Python based passit_sdk in this repo is used for testing the backend only. All cryptography in Passit is done client side in passit-frontend. Compatibility between the testing SDK and actual JS implementation is not guaranteed.

# Development

1. `docker-compose up`
2. In new terminal (without existing docker-compose up), run migrations `docker-compose run --rm web ./manage.py migrate`
3. (optional) load fixtures `docker-compose run --rm web ./manage.py loaddata users groups`

Go to localhost:8000/api to view the api.

## Testing

`docker-compouse run --rm web ./manage.py test`

## Contributing 

You may submit issues to this repo directly or to the passit meta project [here](https://gitlab.com/passit/passit).

# Settings

All settings can be managed with environment variables.

# SSL

Passit is not as secure when run without SSL.

- `IS_DEBUG` - Defaults to False. Leave this as default to be secure.
- `SECURE_SSL_REDIRECT` Defaults to `False`. Set this to True if you'd like Django to redirect HTTP to HTTPS for you. This may not work with all load balancers.
- `SECURE_COOKIE` - Defaults to True. Only set to False is not using SSL

# Celery

Celery is optional. It is recommended only for large scale deployments like app.passit.io
Celery can run tasks like sending email or recording analytics.

- `CELERY_USE_FOR_EMAIL` - Send email using celery. Defaults to False.
- `CELERY_BROKER_URL` - Celery broker (like redis). Example: `redis://redis:6379/0`

## Matomo

Matomo requires running celery. Once celery is configured, set the following env vars:

- `MATOMO_SITE_ID` - Your site's matomo ID. Example: `1`
- `MATOMO_API_URL` - Where you matomo server is hosted. Example: `https://your.site.com/piwik.php`
- `MATOMO_TOKEN_AUTH` - Your matomo API auth (settings, api) Example: `0000000000`
- `MATOMO_TRACK_USER_ID` - Set to True to track user ids. Default is False. https://matomo.org/docs/user-id/

# Logging and error reporting

- `DJANGO_LOG_LEVEL` - Defaults to INFO. Valid settings are 'DEBUG', 'INFO', 'WARNING', 'ERROR', and 'CRITICAL'.

Sentry/Raven error handling support is optional and is really only needed for testing and large scale deployments. To enable set the following:

- `RAVEN_DSN` - Set this to enable Sentry error reporting.
- `ERROR_REPORT_DEFAULT` - Default for user opt in status for client side error reporting. Set to true to make default behavior to send error reports. Defaults to False because sending any client side data to a server carries some risk.
- `ENVIRONMENT` - Note which environment passit is running in (Staging, Production, etc). At this time only sentry makes use of this.

# Other

- `CORS_ORIGIN_ALLOW_ALL` - Only change this if you understand CORS. Defaults to True to allow web extensions to work with Passit. Setting to False will break web extensions unless you compile them yourself to allow your domain.
