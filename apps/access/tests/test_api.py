from django.urls import reverse
from django.test import override_settings
from rest_framework.test import APITestCase
from passit.test_client import PassitTestMixin
from model_bakery import baker
from ..models import User


class TestUsersAPI(PassitTestMixin, APITestCase):
    def test_users_opt_in_error_reporting(self):
        url = reverse('users-list')
        user = baker.make(User)
        self.client.force_login(user)
        res = self.client.get(url)
        self.assertEqual(res.data[0]['opt_in_error_reporting'], False)

    @override_settings(ERROR_REPORT_DEFAULT=True)
    def test_users_default_opt_in_error_reporting(self):
        email = "testy@test.test"
        password = "12345678"
        self.sdk.sign_up(email, password)

        user = User.objects.get()
        url = reverse('users-list')
        self.client.force_login(user)
        res = self.client.get(url)
        self.assertEqual(res.data[0]['opt_in_error_reporting'], True)

    @override_settings(IS_PRIVATE_ORG_MODE=True)
    @override_settings(PRIVATE_ORG_MODE_DOMAIN_WHITELIST=["example.com"])
    def test_private_org_mode_whitelist(self):
        url = reverse('users-list')
        bad_email = "a@notallowed.com"
        fake_data = "a" * 120
        data = {
            "client_salt": "a" * 24,
            "email": bad_email,
            "password": fake_data,
            "private_key": fake_data,
            "private_key_backup": fake_data,
            "public_key": "a" * 44,
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 400)

        good_email = "test@example.com"
        data['email'] = good_email
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 201)

    def test_users_patch_default_opt_in_error_reporting(self):
        user = baker.make(User)
        url = reverse('users-detail', args=[user.id])
        self.client.force_login(user)
        data = {
            'opt_in_error_reporting': True,
        }
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 200)
        user.refresh_from_db()
        self.assertEqual(user.opt_in_error_reporting, True)

    def test_users_patch_email(self):
        """ Users are not allowed to change email """
        user = baker.make(User)
        user_email = user.email
        url = reverse('users-detail', args=[user.id])
        self.client.force_login(user)
        data = {
            'email': 'trick@example.com',
        }
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 200)
        user.refresh_from_db()
        self.assertEqual(res.data['email'], user_email)
        self.assertEqual(user.email, user_email)

    def test_users_patch_password(self):
        """ Users are not allowed to change their password without entering 
        their current password """
        user = baker.make(User)
        user.set_password('hunter2')
        user.save()
        user.refresh_from_db()
        old_password = user.password
        url = reverse('users-detail', args=[user.id])
        self.client.force_login(user)

        data = {
            'password': 'somethingbad',
        }
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 400)

        data = {
            'password': 'somethingbad',
            'current_password': 'wrong',
        }
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 400)
        user.refresh_from_db()
        self.assertEqual(user.password, old_password)

        data = {
            'password': 'somethingbad',
            'current_password': 'hunter2',
        }
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 200)
        user.refresh_from_db()
        self.assertNotEqual(user.password, old_password)

    def test_users_private_key_backup_date(self):
        """ Set the date to when the key is updated """
        user = baker.make(User)
        user.set_password('hunter2')
        user.save()
        user.refresh_from_db()
        self.client.force_login(user)
        url = reverse('users-detail', args=[user.id])
        data = {
            'password': 'hunter2',
            'private_key_backup': 'a' * 120,
        }
        res = self.client.patch(url, data)
        self.assertTrue(res.data['private_key_backup_date'])


class TestUsernameAvailableAPI(APITestCase):
    def test_valid_emails(self):
        url = '/api/username-available/'
        email = "hello@passit.io"
        res = self.client.get(url + email + '/')
        self.assertEqual(res.status_code, 200)
        email = "hello@aa.11"
        res = self.client.get(url + email + '/')
        self.assertEqual(res.status_code, 200)
        email = "bad@11"
        res = self.client.get(url + email + '/')
        self.assertNotEqual(res.status_code, 200)