from django.core import mail
from django.test import TestCase
from model_bakery import baker
from apps.mfa.models import OTP
from ..models import User, GroupUserInvite, Group


class UserTestCase(TestCase):
    def test_mfa_required(self):
        user = baker.make(User)
        self.assertFalse(user.mfa_required)
        baker.make(OTP, user=user, is_active=True)
        self.assertTrue(user.mfa_required)


class GroupUserInviteTestCase(TestCase):
    def test_send_invite_email(self):
        bad_name = '<a href="http://example.com">test</a>'
        group = baker.make(Group, name=bad_name)
        invite = baker.make(GroupUserInvite, invitee_groupuser__group=group)
        invite.send_invite_email()
        email = mail.outbox[-1]
        # Don't allow html injection into email
        self.assertNotIn('href', email.body)
