from rest_framework import serializers


class CodeSerializer(serializers.Serializer):
    email = serializers.EmailField()
    code = serializers.CharField(max_length=80)
    message = serializers.CharField(max_length=80)
