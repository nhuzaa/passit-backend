# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-08-10 23:37
from __future__ import unicode_literals

import apps.confirm_email.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('confirm_email', '0002_auto_20170916_1730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emailconfirmationcode',
            name='short_code',
            field=models.CharField(default=apps.confirm_email.models.create_short_code, help_text='Short human typeable code that must be in hexidecimal', max_length=6),
        ),
    ]
