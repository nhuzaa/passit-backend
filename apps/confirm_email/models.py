import datetime
import secrets

from django.conf import settings
from django.db import models
from django.utils import timezone

from . import conf


def create_short_code():
    """ Create a human typeable code that is short and vulnerable to brute
    force guessing """
    return secrets.token_hex(3)


def create_long_code():
    """ Create a long code for html links that is long enough that it cannot
    be guessed even with a high volume of attempts """
    return secrets.token_urlsafe(40)


def create_expiration():
    return timezone.now() + datetime.timedelta(days=conf.DAYS_TILL_EXPIRE)


class EmailConfirmationCode(models.Model):
    MAX_ATTEMPTS = 10

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=False,
        blank=False
    )
    short_code = models.CharField(
        max_length=6,
        default=create_short_code,
        blank=False,
        null=False,
        help_text="Short human typeable code that must be in hexidecimal",
    )
    long_code = models.CharField(
        max_length=80,  # Note token_urlsafe is bytes, so make this higher
        default=create_long_code,
        blank=False,
        null=False,
        help_text="Long code that isn't easily guessed",
    )
    expiration = models.DateTimeField(
        default=create_expiration,
        blank=False,
        null=False,
    )
    short_code_attempts = models.PositiveSmallIntegerField(default=0)

    @property
    def expired(self):
        # Don't allow more than 10 attempts at guessing
        if self.short_code_attempts > self.MAX_ATTEMPTS:
            return True
        return timezone.now() > self.expiration
