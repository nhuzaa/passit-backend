# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-02-03 00:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ldap', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ldapsync',
            options={'verbose_name': 'LDAP Sync Setting'},
        ),
        migrations.AddField(
            model_name='ldapsync',
            name='email_domain',
            field=models.CharField(default='', help_text='Check users with this email domain against this LDAP server. Example: `example.com` would make the user person@example.com check against this LDAP server.', max_length=254),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='ldapsync',
            name='base_dn',
            field=models.CharField(help_text='Base DN for LDAP search when looking up users. Example: ou=people,dc=example,dc=com', max_length=1000),
        ),
        migrations.AlterField(
            model_name='ldapsync',
            name='bind_password',
            field=models.CharField(blank=True, help_text='Password for bind_user. Note this is saved unencrypted.', max_length=1000),
        ),
        migrations.AlterField(
            model_name='ldapsync',
            name='bind_user',
            field=models.CharField(help_text='LDAP Binding Account to authenticate to server to. Preferably this account should have read only access. Example: cn=my-user,dc=example,dc=com', max_length=1000),
        ),
        migrations.AlterField(
            model_name='ldapsync',
            name='email_attribute',
            field=models.CharField(blank=True, help_text='LDAP attribute to match to Passit email address. Example: mail. If left blank, Passit will guess the value.', max_length=100),
        ),
        migrations.AlterField(
            model_name='ldapsync',
            name='filters',
            field=models.CharField(blank=True, help_text='Filter for LDAP search. Example: (objectclass=person)', max_length=1000),
        ),
        migrations.AlterField(
            model_name='ldapsync',
            name='first_name_attribute',
            field=models.CharField(blank=True, help_text='LDAP attribute to match to Passit first name (given name). Example: gn. If left blank, Passit will guess the value', max_length=100),
        ),
        migrations.AlterField(
            model_name='ldapsync',
            name='last_name_attribute',
            field=models.CharField(blank=True, help_text='LDAP attribute to match to Passit last name (surname). Example: sn. If left blank, Passit will guess the value', max_length=100),
        ),
        migrations.AlterField(
            model_name='ldapsync',
            name='ldap_uri',
            field=models.CharField(help_text='Example: ldap://ldap.example.com', max_length=1000),
        ),
    ]
