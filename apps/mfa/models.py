from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from knox.models import AuthToken
import pyotp


class OTP(models.Model):
    """ One time password secret """
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    secret = models.CharField(max_length=50)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    last_otp = models.CharField(
        max_length=50,
        blank=True,
        help_text="Deny replay attacks",
    )
    verified_tokens = models.ManyToManyField(AuthToken, through='OTPVerifiedToken', blank=True)
    
    @staticmethod
    def generate(user) -> str:
        secret = pyotp.random_base32()
        otp = OTP.objects.create(user=user, secret=secret)
        uri = pyotp.totp.TOTP(secret).provisioning_uri(user.email, issuer_name="Passit")
        return {
            "provisioning_uri": uri,
            "id": otp.id
        }
    
    def verify_otp(self, otp: str) -> bool:
        """ Check if the OTP is valid """
        if pyotp.utils.strings_equal(otp, self.last_otp):
            return False
        totp = pyotp.TOTP(self.secret)
        return totp.verify(otp)

    def verify(self, otp: str, auth_token) -> bool:
        """
        Verify OTP and deny using the most recent otp to prevent replay attacks
        Mark the auth_token as verified
        """
        is_valid = self.verify_otp(otp)
        if is_valid:
            self.last_otp = otp
            self.save()
            if OTPVerifiedToken.objects.filter(otp=self, auth_token=auth_token).exists():
                # User is already verified
                return False
            OTPVerifiedToken.objects.create(otp=self, auth_token=auth_token)
        return is_valid


class OTPVerifiedToken(models.Model):
    otp = models.ForeignKey(OTP, related_name="otp_verifications", on_delete=models.CASCADE)
    auth_token = models.ForeignKey(AuthToken, related_name="otp_verifications", on_delete=models.CASCADE)

    class Meta:
        unique_together = ('otp', 'auth_token')
