from django.urls import reverse
from django.test import Client
from django.utils import timezone
from model_bakery import baker
from rest_framework.test import APITestCase
from ...access.models import User, Group, GroupUser, BackupCodeEmailToken
from ..models import Secret, SecretThrough
from ..views import SecretThroughViewSet
import json
import datetime


class TestSecretThroughViewSet(APITestCase):
    def test_create_secret_through(self):
        """
        user1 creates a group and puts a secret in that group
        user2 POSTS to that secret's API URL (/secrets/<id>/groups/)
            with valid data, is unsuccessful
        """
        user1 = baker.make(User)
        user1_group = baker.make(Group)
        user1_secret = baker.make(Secret, data={"password": "123456"})
        user1_secret_through = baker.make(SecretThrough, secret=user1_secret,
                                          user=user1, group=user1_group,
                                          data={"password": "123456"})
        user2 = baker.make(User)
        user2_group = baker.make(Group)
        user2_secret = baker.make(Secret, data={"password": "asdfgh"})
        user2_secret_through = baker.make(SecretThrough, secret=user2_secret,
                                          user=user2, group=user2_group,
                                          data={"password": "asdfgh"})
        user2_request = '{"id": ' + str(user2_secret.id) + ', "group": ' + str(user2_group.id) + ', "key_ciphertext": "a", "data": {"password": "zxcvbn"}, "public_key": "a", "is_mine": true}'

        client = Client()
        client.force_login(user2)

        post_to_users_secret = client.post("/api/secrets/" + str(user2_secret.id) + "/groups/",
                                           user2_request,
                                           content_type="application/json")

        self.assertEqual(post_to_users_secret.status_code, 201)

        post_to_another_users_secret = client.post("/api/secrets/" + str(user1_secret.id) + "/groups/",
                                                   user2_request,
                                                   content_type="application/json")

        self.assertEqual(post_to_another_users_secret.status_code, 403)

        delete_attempt = client.delete(f'/api/secrets/{user1_secret.id}/groups/{user1_secret_through.id}/')
        self.assertEqual(delete_attempt.status_code, 404)


class TestChangePasswordView(APITestCase):
    def test_change_password_private_key_backup(self):
        """
        Can optionally change private key backup when changing password at the
        same time.
        """
        url = reverse('change-password')
        user_pass = 'cool'
        data = {
            'secret_through_set': [],
            'group_user_set': [],
            'user': {
                'client_salt': 'a' * 24,
                'password': 'a',
                'private_key': 'a' * 120,
                'public_key': 'a' * 44,
                'private_key_backup': 'a' * 120,
            },
            'old_password': user_pass
        }
        user = baker.make(User)
        user.set_password(user_pass)
        user.save()
        self.client.force_login(user)
        res = self.client.post(url, json.dumps(data), content_type='application/json')
        self.assertEqual(res.status_code, 200)
        # Contains auth token
        self.assertTrue(res.data['token'])
        user.refresh_from_db()
        # Able to update private_key_backup
        self.assertEqual(user.private_key_backup, 'a' * 120)
        self.assertEqual(res.data['user']['private_key'], 'a' * 120)


class ChangePasswordBackupViewSet(APITestCase):
    def test_change_password_backup_failure(self):
        """ Change password backup should require user to be logged in
        and to have a backup email token """
        url = reverse('change-password-backup')
        data = {
            'secret_through_set': [],
            'group_user_set': [],
            'code': 'a',
            'user': {
                'client_salt': 'a' * 24,
                'password': 'a',
                'private_key': 'a' * 120,
                'public_key': 'a' * 44,
                'private_key_backup': 'a' * 120,
            }
        }

        # Must be logged in
        res = self.client.post(url, json.dumps(data), content_type='application/json')
        self.assertEqual(res.status_code, 403)

        user = baker.make(User)
        self.client.force_login(user)
        res = self.client.post(url, json.dumps(data), content_type='application/json')
        # Incorrect code
        self.assertEqual(res.status_code, 400)

        expire_date = timezone.now() + datetime.timedelta(days=1)
        code = baker.make(BackupCodeEmailToken, user=user, expiration=expire_date)
        data['code'] = code.code
        res = self.client.post(url, json.dumps(data), content_type='application/json')
        # Finally valid
        self.assertEqual(res.status_code, 200)
